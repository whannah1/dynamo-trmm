load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"

begin

	;dir = "/media/Helios/DYNAMO/TRMM"
	dir = "/media/Helios/DYNAMO/TRMM"
	
	idir = dir+"/raw"
	odir = dir+"/"

	mn = (/09,10,11,12/)
	yr = (/11,11,11,11/)+2000
	
	;mn = (/06,07,08,09,10,11,12,01,02,03,04/)
	;yr = (/11,11,11,11,11,11,11,12,12,12,12/)+2000
		
	lat1 = -40.
	lat2 =  40.
		
	ivar = "pcp"
	ovar = "precip"
	
	ofile = odir+"TRMM.DYNAMO.daily.filtered.nc"
	
	create_file = True

;===================================================================================
;===================================================================================
  
  days_per_month = (/31,28,31,30,31,30,31,31,30,31,30,31/)

  list_string = ""
  num_m = dimsizes(mn)
  do m = 0,num_m-1
    list_string = list_string + idir+"/3B42."+yr(m)+sprinti("%0.2i",mn(m))+"*.nc  "
  end do
  ifile = systemfunc("ls "+list_string)
  num_f = dimsizes(ifile)
  
    if create_file then
     if isfilepresent(ofile) then
       system("rm "+ofile)
     end if
     outfile = addfile(ofile,"c")
     outfile@start_date = yr(0)    +"-"+mn(0)    +"-01"
     outfile@end_date   = yr(num_m-1)+"-"+mn(num_m-1)+"-"+days_per_month(mn(num_m-1)-1)
     outfile@sample_freq = "3-hour"
    else
     outfile = addfile(ofile,"w")
    end if
  
  
  
  do f = 0,num_f-1    
    infile = addfile(ifile(f),"R")
    if f.eq.0 then
    if create_file then
      print(infile)
      lat = infile->latitude({lat1:lat2})
      lon = infile->longitude
      num_lat = dimsizes(lat)
      num_lon = dimsizes(lon)
      print("allocating output variable")
      oV = new((/num_f,num_lat,num_lon/),float)
      	oV!0 = "hour"
	oV!1 = "lat"
	oV!2 = "lon"
	oV&lat = lat
	oV&lon = lon
	oV@units = "mm/day"
	oV@long_name = "precipitation"
      print("writing to file")
      outfile->$ovar$ = oV
        delete(oV)
    end if
    end if
    ;print("f = "+f+"	("+(num_f-1)+")")
    outfile->$ovar$(f,:,:) = infile->$ivar$(:,{lat1:lat2},:) * 24.	; convert from mm/hr to mm/day
  end do
  
  print("")
  print("  "+ofile)
  print("")
;===================================================================================
;===================================================================================
end
