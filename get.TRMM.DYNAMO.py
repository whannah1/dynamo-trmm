#!/usr/bin/env python
#========================================================================================================================
# 	This script retrieves TRMM 3B42 data	
# 	
#    Nov, 2013	Walter Hannah 		Colorado State University
#========================================================================================================================
import datetime
import sys
import os
import numpy as np
#========================================================================================================================
#========================================================================================================================

#linkfile = "/maloney-scratch/whannah/DYNAMO/TRMM/TRMM.3hour.LinkList.DYNAMO.2011.1001-1215.dat"
#linkfile = "/maloney-scratch/whannah/DYNAMO/TRMM/TRMM.3hour.LinkList.DYNAMO.2011.09-12.dat"
linkfile = "~/Research/DYNAMO/TRMM/TRMM.3hour.LinkList.DYNAMO.2011-07_2012-04.dat"

#odir  = "/media/Helios/DYNAMO/TRMM/raw/"
odir  = "/maloney-scratch/whannah/DYNAMO/TRMM/raw_data/"

os.system("wget -nc --content-disposition -i "+linkfile+" -P "+odir)

print
print odir
print

#========================================================================================================================
#========================================================================================================================


