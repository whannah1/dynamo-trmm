load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin
    
    fig_type = "x11"
    fig_file = "~/Research/DYNAMO/TRMM/test.swath.v1"
    
    debug = False
    
;====================================================================================================
; Load Data
;====================================================================================================
    ;ifile = "~/Research/DYNAMO/TRMM/2A25.101021.73661.6.HDF"
    ifile = "~/Research/DYNAMO/TRMM/2A23.20111021.79350.7.HDF"

    infile = addfile(ifile,"r")
    lat = infile->Latitude
    lon = infile->Longitude
    nscan = dimsizes(lat(:,0))
    nray  = dimsizes(lat(0,:))
    
    ht = new((/nscan,nray/),float)
    ht = tofloat( infile->stormH  )

    ;ht = where(ht.eq.-9999,ht@_FillValue,ht)
    ;ht = where(ht.eq.-8888,ht@_FillValue,ht)
    ;ht = where(ht.eq.-1111,ht@_FillValue,ht)

    ht = where(ht.gt.0.,random_uniform(-1,1,(/nscan,nray/)),ht)

    ht@lat2d = lat
    ht@lon2d = lon
;====================================================================================================
;====================================================================================================    
    if debug then 
        print(infile)
        printline()
        printMAM( ht )
        printline()
        printVarSummary(ht)
        printline()
    end if
;====================================================================================================
;====================================================================================================
    wks = gsn_open_wks(fig_type,fig_file)
        res = setres_contour()
        res@mpMinLatF       = min( (/min(lat), -40/) )     
        res@mpMaxLatF       = max( (/max(lat),  40 /) )
        res@mpMinLonF       = min(lon) 
        res@mpMaxLonF       = max(lon)
        res@mpCenterLonF    = 180.
        ;res@cnFillMode      = "RasterFill"
        res@cnFillMode      = "CellFill"

    plot = gsn_csm_contour_map_ce(wks,ht, res)

;====================================================================================================
;====================================================================================================
    pres = True

    ;gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)
    gsn_panel(wks,plot,(/1,dimsizes(plot)/),pres)

  
    print("")
    print(" "+fig_file+"."+fig_type)
    print("")
    trimPNG(fig_file)
;====================================================================================================
;====================================================================================================
end
 